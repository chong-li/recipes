import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# reads data e cleans species column
iris = pd.read_csv("iris.csv")
sp_dict = {"Iris-setosa": 1,
            "Iris-versicolor": 2,
            "Iris-virginica": 3}
iris = iris.replace(sp_dict)
species = iris.species
iris = iris.drop(columns = "species")

# create figure for plotting
fig = plt.figure()
# scatter plots two different columns, can be switch by any others
actual = fig.add_subplot(221)
actual.scatter(iris["sepal_length"], iris["sepal_width"], c = species)

# run K-means on data as is
#from sklearn.cluster import KMeans
#km = KMeans(n_clusters = 3, random_state = 42)
#k = km.fit_predict(iris)

# run GMM on data as is
from sklearn.mixture import GaussianMixture
gm = GaussianMixture(n_components = 3, random_state = 42)
gm.fit(iris)
k = gm.predict_proba(iris)

# plots clustering results
predicted = fig.add_subplot(222)
predicted.scatter(iris["sepal_length"], iris["sepal_width"], c = k)

# apply pca on data
from sklearn.decomposition import PCA
pca_iris = PCA().fit_transform(iris)
pca_df = pd.DataFrame(pca_iris)

# plot actual data on 1st and 2nd principal dimensions
actual_pca = fig.add_subplot(223)
actual_pca.scatter(pca_df[0], pca_df[1], c = species)

# run K-means on transformed data
#k_pca = km.fit_predict(pca_df)

# run GMM on transformed data
gm.fit(iris)
k_pca = gm.predict_proba(iris)

# plot clustering results on transformed data
predicted_pca = fig.add_subplot(224)
predicted_pca.scatter(pca_df[0], pca_df[1], c = k_pca)


plt.show()
